using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour
{
    public float playerJumpForce = 20f;
    public float playerSpeed = 5f;
    public Sprite[] mySprites;
    private int index = 0;

    private Rigidbody2D myrigidbody2D;
    private SpriteRenderer myspriteRenderer;
    public GameObject Bullet;

    public GameManager myGameManager;


    public GameObject gameOverPanel; // Referencia al panel que contiene el mensaje y el bot�n de reinicio
    public Button replayButton; // Referencia al bot�n de reinicio



    // Start is called before the first frame update
    void Start()
    {
        myrigidbody2D = GetComponent<Rigidbody2D>();
        myspriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(WalkCoRutine());
        myGameManager = FindObjectOfType<GameManager>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            myrigidbody2D.velocity = new Vector2(myrigidbody2D.velocity.x, playerJumpForce);

        }
        myrigidbody2D.velocity = new Vector2(playerSpeed, myrigidbody2D.velocity.y);
        if (Input.GetKeyDown(KeyCode.E))
        {
            Instantiate(Bullet, transform.position, Quaternion.identity);
        }
    }


    IEnumerator WalkCoRutine()
    {
        yield return new WaitForSeconds(0.05f);
        myspriteRenderer.sprite = mySprites[index];
        index++;
        if (index == 6)
        {
            index = 0;
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ItemGood"))
        {
            Destroy(collision.gameObject);
            myGameManager.AddScore();
        }
        else if (collision.CompareTag("ItemBad"))
        {
            Destroy(collision.gameObject);
            PlayerDeath();
        }
        else if (collision.CompareTag("DeathZone"))
        {
            PlayerDeath();
        }
    }

    void PlayerDeath()
    {
        SceneManager.LoadScene("Level2D");
        gameOverPanel.SetActive(true); // Muestra el panel de game over
        Time.timeScale = 0; // Detiene el tiempo en el juego, paus�ndolo
        replayButton.onClick.AddListener(RestartGame); // A�ade el listener al bot�n de reinicio
    }

    void RestartGame()
    {
        Time.timeScale = 1; // Reanuda el tiempo del juego
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); // Carga la escena actual para reiniciar el juego
    }
}
