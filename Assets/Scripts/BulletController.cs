using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private Rigidbody2D myrigidbody2D;
    public float bulletSpeed = 10f;
    public GameManager myGameManager;


    // Start is called before the first frame update
    void Start()
    {
        myrigidbody2D = GetComponent<Rigidbody2D>();
        myGameManager = FindObjectOfType<GameManager>();
        if (myGameManager == null)
        {
            Debug.LogError("GameManager not found in the scene!");
        }

    }

    // Update is called once per frame
    void Update()
    {
        myrigidbody2D.velocity = new Vector2(bulletSpeed, myrigidbody2D.velocity.y);

    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        //if (collision.CompareTag("ItemGood"))
        //{
            //Destroy(collision.gameObject);
            
        //}
        if (collision.CompareTag("ItemBad"))
        {
            if (myGameManager != null)
            {
                myGameManager.AddScore();
            }
            else
            {
                Debug.LogError("GameManager reference is null.");
            }

            myGameManager.AddScore();
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
