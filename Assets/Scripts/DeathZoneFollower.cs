using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZoneFollower : MonoBehaviour
{
    public Transform targetPlayer;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(targetPlayer.position.x + 6f, -10f, 0);
    }
}
